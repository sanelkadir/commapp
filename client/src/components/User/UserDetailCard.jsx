import React from "react";

const UserDetailCard = () => {
  return (
    <div class="card border-0 shadow">
      <div class="card-body">
        <h5 class="card-title mb-2">Detaylar</h5>
        {/* <button className="btn btn-purple-200 border-0 text-purple-800 text-start w-100 mb-2">Hakkında Ekle</button> */}
        <div className="card mb-2">
          <div className="card-body">
            <h6 class="card-subtitle mb-2">Hakkında</h6>
            <p className="text-truncate">
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum has been the industry's standard dummy text
              ever since the 1500s, when an unknown printer took a galley of
              type and scrambled it to make a type specimen book. It has
              survived not only five centuries, but also the leap into
              electronic typesetting, remaining essentially unchanged. It was
              popularised in the 1960s with the release of
            </p>
          </div>
        </div>
        <button className="btn btn-purple-200 border-0 text-purple-800 text-start w-100 mb-2">
          Okul Ekle
        </button>
        <button className="btn btn-purple-200 border-0 text-purple-800 text-start w-100 mb-2">
          Çalıştığın Yeri Ekle
        </button>
        <button className="btn btn-purple-200 border-0 text-purple-800 text-start w-100 mb-2">
          Topluluk Kur
        </button>
      </div>
    </div>
  );
};

export default UserDetailCard;
