import React from "react";

const CommForm = () => {
  return (
    <>
      <div className="row">
        <div className="col-3"></div>
        <div className="col-lg-6">
          <form>
            <div class="mb-2">
              <input
                type="email"
                class="form-control"
                placeholder="Kullanıcı Adı"
              />
            </div>
            <div class="mb-2">
              <input
                type="email"
                class="form-control"
                placeholder="E-Posta Adresi"
              />
            </div>
            <div class="mb-2">
              <input
                type="password"
                class="form-control"
                placeholder="Parola"
              />
            </div>
            <div class="mb-2">
              <input
                type="password"
                class="form-control"
                placeholder="Parola Tekrarı"
              />
            </div>
            <button type="submit" class="btn btn-purple-700">
              Kaydol
            </button>
          </form>
        </div>
        <div className="col-3"></div>
      </div>
    </>
  );
};

export default CommForm;
