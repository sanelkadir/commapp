import React, { useState } from "react";
import { Link } from "react-router-dom";

import Auth from "../../modules/Auth/Auth";

const HomeNavbar = () => {
  let auth = new Auth();
  const [menu, setMenu] = useState("home");

  async function handleLogOut() {
    await auth.logOut();
    window.location.href = "/";
  }

  return (
    <>
      <div className="container-fluid bg-purple-700 sticky-lg-top">
        <div className="container">
          <header class="d-flex flex-wrap align-items-center justify-content-center justify-content-md-between">
            <a
              href="/"
              class="d-flex align-items-center col-md-3 mb-2 mb-md-0 text-purple-100 text-decoration-none fs-3"
            >
              <i class="fa-solid fa-crow fs-2"></i>Crow
            </a>

            <ul class="nav col-12 col-md-auto mb-2 justify-content-center mb-md-0">
              <li class="nav-item">
                <Link
                  onClick={() => setMenu("home")}
                  class={
                    menu === "home"
                      ? "btn btn-purple-700 border-0 fs-4 me-5 border-bottom border-purple-100  border-5"
                      : "btn btn-purple-700 border-0 fs-4 me-5 text-purple-300"
                  }
                  to="/"
                >
                  <i class="fa-solid fa-house"></i>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={() => setMenu("communities")}
                  class={
                    menu === "communities"
                      ? "btn btn-purple-700 border-0 fs-4 me-5 border-bottom border-purple-100  border-5"
                      : "btn btn-purple-700 border-0 fs-4 me-5 text-purple-300"
                  }
                  to="/communities"
                >
                  <i class="fa-solid fa-users"></i>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={() => setMenu("messages")}
                  class={
                    menu === "messages"
                      ? "btn btn-purple-700 border-0 fs-4 me-5 border-bottom border-purple-100  border-5"
                      : "btn btn-purple-700 border-0 fs-4 me-5 text-purple-300"
                  }
                  to="/messages"
                >
                  <i class="fa-solid fa-comments"></i>
                </Link>
              </li>
              <li class="nav-item">
                <Link
                  onClick={() => setMenu("cloud")}
                  class={
                    menu === "cloud"
                      ? "btn btn-purple-700 border-0 fs-4 me-5 border-bottom border-purple-100  border-5"
                      : "btn btn-purple-700 border-0 fs-4 me-5 text-purple-300"
                  }
                  to="/cloud"
                >
                  <i class="fa-solid fa-cloud"></i>
                </Link>
              </li>
            </ul>
            <div class="col-md-3 text-end">
              <button class="btn btn-purple-700 border-0 fs-4 me-2 text-purple-300">
                <i class="fa-solid fa-gears"></i>
              </button>
              <button
                class="btn btn-purple-700 border-0 fs-4 text-purple-300"
                onClick={handleLogOut}
              >
                <i class="fa-solid fa-right-from-bracket"></i>
              </button>
            </div>
          </header>
        </div>
      </div>
    </>
  );
};

export default HomeNavbar;
