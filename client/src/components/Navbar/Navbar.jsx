import React from "react";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <nav class="navbar navbar-expand-lg bg-indigo-800 navbar-dark">
        <div class="container-fluid">
          <a class="navbar-brand fs-1 fst-italic p-0 me-5" to="#">
            Crow
          </a>
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
              <li class="nav-item">
                <Link class="nav-link" to="">
                  Özellikler
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="">
                  Fiyatlandırma
                </Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to="">
                  Destek
                </Link>
              </li>
            </ul>
            <form class="d-flex" role="search">
              <Link to="/auth/login" class="btn btn-outline-indigo-200 me-3">
                Giriş Yap
              </Link>
              <Link to="/auth/register" class="btn btn-indigo-200">
                Kaydol
              </Link>
            </form>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Navbar;
