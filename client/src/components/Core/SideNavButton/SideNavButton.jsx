import React from "react";
import { Link } from "react-router-dom";

const SideNavButton = ({icon, name, link}) => {
  return (
    <>
      <Link className="btn btn-outline-purple-100 border-0 scale-up-hor-center text-purple-800 text-start w-100 mt-2  p-2" to={link}>
        <div className="row">
          <div className="col-lg-2">
            <i className={icon + " fs-3"}></i>
          </div>
          <div className="col-lg-10">
            <span className="">{name}</span>
          </div>
        </div>
      </Link>
    </>
  );
};

export default SideNavButton;
