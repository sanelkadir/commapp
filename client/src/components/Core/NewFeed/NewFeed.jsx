import React, { useState } from "react";

const NewFeed = () => {
  const [feedType, setFeedType] = useState(null);
  return (
    <>
      <div class="card border-0 shadow mb-3">
        <div class="card-body">
          <div className="row">
            <div className="col-lg-1">
              <img
                src="https://picsum.photos/200"
                className="img-fluid rounded-circle image-custom"
                alt="..."
              />
            </div>
            <div className="col-lg-11">
              <div class="">
                <textarea
                  type="email"
                  class="form-control mb-2 rounded-pill"
                  id="newFeed"
                  placeholder="Buraya yazınız"
                ></textarea>
              </div>
              <div className="row">
                <div className="col-4">
                  <select
                    class="form-select rounded-pill"
                    aria-label="Default select example"
                    onChange={(e) => setFeedType(e.target.value)}
                  >
                    <option selected>Görünürlük</option>
                    <option value="1">Topluluğa Özel</option>
                    <option value="2">Tüm Topluluklara</option>
                  </select>
                </div>
                <div className="col-4">
                  {feedType === "1" ? (
                    <>
                      <select
                        class="form-select rounded-pill"
                        aria-label="Default select example"
                        onChange={(e) => setFeedType(e.target.value)}
                      >
                        <option selected>Topluluk Seç</option>
                        <option value="1">One</option>
                        <option value="2">Two</option>
                        <option value="3">Three</option>
                      </select>
                    </>
                  ) : (
                    <></>
                  )}
                </div>
                <div className="col-4">
                  <button className="btn btn-purple-600 text-white w-100 rounded-pill">
                    Paylaş
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default NewFeed;
