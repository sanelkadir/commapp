import React from "react";

const FeedCard = () => {
  return (
    <>
      <div class="card border-0 shadow mb-3">
        <div class="card-body">
          <div className="row">
            <div className="col-1">
              <img
                src="https://picsum.photos/200"
                className="img-fluid rounded-circle image-custom"
                alt="..."
              />
            </div>
            <div className="col-11">
              <div className="col-12 p-0 m-0">Kadir Umut Şanel </div>
              <div className="col-12 p-0 m-0">
                <small className="text-muted">22 Haziran 2022</small>
              </div>
            </div>
          </div>
          <div className="row mt-2 mb-0">
            <p>
              ‘’Baba bana muz alır mısın?’’ dedi. Adam sessizce ‘’Söz kızım para
              kalırsa bu hafta alacağım sana’’ deyip ilerledi, ama tam
              arkasındaki beni farketmedi. Pazarcı abiye dedim ki "Bu adam ile
              çocuğuna iyi bak. Şimdi 2 kilo muz tart. Birazdan senin tezgahın
              önünden geçerse ve durup muz almazsa abi diye seslen. Sonra ona "
              Hani geçen hafta bozuk yok diye para üstü verememiştim ya.
            </p>
          </div>
          <hr className="mt-0 mb-1" />
          <div className="row">
            <div className="col-lg-6">
              <button className="btn btn-outline-purple-100 border-0 text-purple-800 text-center w-100"><i class="fa-solid fa-heart"></i> Beğen</button>
            </div>
            <div className="col-lg-6">
              <button className="btn btn-outline-purple-100 border-0 text-purple-800 text-center w-100"><i class="fa-solid fa-paper-plane"></i> Paylaş</button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FeedCard;
