import React from "react";
import { Link } from "react-router-dom";

import "./sideShortcutButton.css";

const SideShortcutButton = ({ link, picture, name }) => {
  return (
    <>
      <Link
        className="btn btn-outline-purple-100 border-0 scale-up-hor-center text-purple-800 text-start w-100 mt-2"
        to={link}
      >
        <div className="row">
          <div className="col-lg-3">
            <img src={picture} className="img-fluid rounded image-custom" alt="..." />
          </div>
          <div className="col-lg-9">
            <span className="text-truncate text-truncate--2">{name}</span><br />
          </div>
        </div>
      </Link>
    </>
  );
};

export default SideShortcutButton;
