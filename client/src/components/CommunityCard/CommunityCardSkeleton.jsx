import React from "react";

const CommunityCardSkeleton = () => {
  return (
    <>
      <div className="col-lg-6">
        <div class="card mb-4 border-0 shadow  text-purple-800">
          <div class="row g-0">
            <div class="col-md-4 p-2 placeholder-glow">
              <div className="w-100 h-100 placeholder">
                <span></span>
              </div>
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title placeholder-glow"><span class="placeholder col-6"></span></h5>
                <p class="card-text placeholder-glow">
                  <span class="placeholder col-7"></span>
                  <span class="placeholder col-4"></span>
                  <span class="placeholder col-4"></span>
                  <span class="placeholder col-6"></span>
                  <span class="placeholder col-8"></span>
                </p>
                <div className="row">
                  <div className="col-lg-6">
                    <p class="card-text placeholder-glow">
                      <span className="placeholder col-6"></span>
                    </p>
                  </div>
                  <div className="col-lg-6">
                    <button className="btn btn-purple-700 w-100 disabled placeholder col-6"></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CommunityCardSkeleton;
