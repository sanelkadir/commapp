import React from "react";

import "./communityCard.css"

const CommunityCard = () => {
  return (
    <>
      <div className="col-lg-6">
        <div class="card mb-4 border-0 shadow  text-purple-800">
          <div class="row g-0">
            <div class="col-md-4 p-2">
              <img
                src="https://raclab.org/wp-content/uploads/2021/07/raclab.png"
                class="img-thumbnail border-0 rounded-start"
                alt="..."
              />
            </div>
            <div class="col-md-8">
              <div class="card-body">
                <h5 class="card-title">Raclab</h5>
                <p class="card-text text-truncate text-truncate--3">
                  RACLAB bünyesinde mesleki ve iletişimsel becerilerini
                  geliştiren öğrenciler, akademik çalışmalar da yaparak
                  CV’lerini zenginleştirmektedirler. RACLAB bünyesinde mesleki ve iletişimsel becerilerini
                  geliştiren öğrenciler, akademik çalışmalar da yaparak
                  CV’lerini zenginleştirmektedirler.
                </p>
                <div className="row">
                  <div className="col-lg-6">
                  <p class="card-text">
                  <small class="text-muted">Üye Sayısı : 230</small>
                </p>
                  </div>
                  <div className="col-lg-6">
                    <button className="btn btn-purple-700 w-100">Katıl</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CommunityCard;
