import React from "react";

// import CommunityCard from "../../components/CommunityCard/CommunityCard";
import CommunityCardSkeleton from "../../components/CommunityCard/CommunityCardSkeleton";
import CommForm from "../../components/CommForm/CommForm";

const Landing = () => {
  return (
    <>
      <div className="bg-purple-200 rounded p-5 border border-0 shadow mb-5">
        <h5 className="text-purple-900">Topluluğunu mu arıyorsun?</h5>
        <div class="input-group">
          <input
            type="text"
            class="form-control border-0"
            placeholder="Örneğin: Yazılım Topluluğu"
          />
          <button class="btn btn-purple-400" type="button" id="button-addon2">
            <i class="fa-solid fa-magnifying-glass text-purple-100"></i>
          </button>
        </div>
      </div>
      <div className="row mb-5">
        <h3>Bir topluluğa katılmak ister misin?</h3>
        <hr className="text-purple-300" />
        <CommunityCardSkeleton />
        <CommunityCardSkeleton />
        <CommunityCardSkeleton />
        <CommunityCardSkeleton />
      </div>
      <div className="row">
        <h5>Ya da bir hesap aç ve kendi topluluğunu kur.</h5>
        <hr className="text-purple-300" />
        <CommForm></CommForm>
      </div>
    </>
  );
};

export default Landing;
