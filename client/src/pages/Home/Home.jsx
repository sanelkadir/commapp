import React from "react";

import NewFeed from "../../components/Core/NewFeed/NewFeed";
import FeedCard from "../../components/Core/Feed/FeedCard";

const Home = () => {
  return (
    <>
      <div className="container px-5 mt-3">
        <NewFeed></NewFeed>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
        <FeedCard></FeedCard>
      </div>
    </>
  );
};

export default Home;
