import React from "react";
import { Link } from "react-router-dom";

const Register = () => {
  return (
    <>
      <div class="login-wrap">
        <div className="card p-5">
          <Link to="/" class="text-center text-decoration-none display-3 fst-italic fw-bold text-purple-700 mb-4">Crowd</Link>
          <div className="row mb-3">
            <div className="col-lg-6">
              <input
                type="text"
                class="form-control"
                placeholder="Adınız"
              />
            </div>
            <div className="col-lg-6">
              <input
                type="text"
                class="form-control"
                placeholder="Soyadınız"
              />
            </div>
          </div>
          <div class="input-group mb-3">
            <span
              class="input-group-text bg-purple-400 text-white"
              id="basic-addon1"
            >
              @
            </span>
            <input
              type="text"
              class="form-control"
              placeholder="Kullanıcı Adı"
            />
          </div>
          <div class="input-group mb-3">
            <input type="email" class="form-control" placeholder="E-Posta" />
          </div>
          <div class="input-group mb-3">
            <input type="password" class="form-control" placeholder="Parola" />
          </div>
          <button className="btn btn-purple-400 text-white w-100">
            <i class="fa-solid fa-right-to-bracket"></i> Kaydol
          </button>
          <div className="row text-light mt-4">
            <div className="col-5">
              <hr />
            </div>
            <div className="col-2 text-center text-purple p-0 m-0">Ya Da</div>
            <div className="col-5">
              <hr/>
            </div>
          </div>
          <center>
            <div className="row">
              <div className="col-12 mt-3">
                <a href="/auth/google" className="btn btn-blue-800 fs-5">
                  <i class="fa-brands fa-google-plus-square fs-3 me-2"></i>{" "}
                  Google ile Kaydol
                </a>
              </div>
              <div className="col-12 mt-3">
                <a href="/auth/forgetpassword" className="text-purple-400">
                  Şifreni mi unuttun?
                </a>
              </div>
            </div>
          </center>
        </div>
        <div className="card p-4 mt-1">
          <h6 className="text-purple-400 text-center p-0 m-0">
            Zaten üye misin?{" "}
            <a
              href="/auth/login"
              className="text-purple-700 text-decoration-none"
            >
              Giriş Yap
            </a>
          </h6>
        </div>
      </div>
    </>
  );
};

export default Register;
