import React, { useState } from "react";
import { Link } from "react-router-dom";

import Feed from "./Feed";
import CommList from "./CommList"
import FriendList from "./FriendList"
import Media from "./Media"
import EventList from "./EventList"

const Profile = () => {
  const [menu, setMenu] = useState("feed");

  return (
    <>
      <div className="bg-purple-100 pt-3 ">
        <div className="container">
          <div className="row">
            <div className="container">
              <div className="row">
                <div className="col-lg-2">
                  <img
                    src="https://raclab.org/wp-content/uploads/2021/07/raclab.png"
                    class="img-thumbnail border-0 rounded-circle"
                    alt="..."
                  />
                </div>
                <div className="col-lg-8 mt-5">
                  <h1>Kadir Umut Sanel</h1>
                  <span className="text-muted">7 topluluğa üye</span>
                </div>
                <div className="col-lg-2">
                  <button className="btn btn-purple-800 mb-2 w-100">
                    Profili Düzenle
                  </button>
                  <button className="btn btn-purple-800 mb-2 w-100">
                    Topluluğa Gönderi Ekle
                  </button>
                  <button className="btn btn-purple-800 mb-2 w-100">
                    Topluluklarını Yönet
                  </button>
                  <button className="btn btn-purple-800 w-100">
                    Planını Yükselt
                  </button>
                </div>
              </div>
              <hr />
              <div className="row">
                <div className="col-3"></div>
                <div className="col-6">
                  <div className="row">
                    <div className="col">
                      <button
                        onClick={() => setMenu("feed")}
                        className={
                          menu === "feed"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Gönderiler
                      </button>
                      <button
                        onClick={() => setMenu("communities")}
                        className={
                          menu === "communities"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Topluluklar
                      </button>
                      <button
                        onClick={() => setMenu("friends")}
                        className={
                          menu === "friends"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Arkadaşlar
                      </button>
                      <button
                        onClick={() => setMenu("media")}
                        className={
                          menu === "media"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Medya
                      </button>
                      <button
                        onClick={() => setMenu("events")}
                        className={
                          menu === "events"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Etkinlikler
                      </button>
                      <button
                        onClick={() => setMenu("chat")}
                        className={
                          menu === "chat"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Mesaj
                      </button>
                      <button
                        onClick={() => setMenu("cloud")}
                        className={
                          menu === "cloud"
                            ? "btn btn-purple-100 border-0 border-bottom border-purple-800 border-5"
                            : "btn btn-purple-100 border-0 border-5 border-bottom border-purple-100"
                        }
                      >
                        Bulut
                      </button>
                    </div>
                  </div>
                </div>
                <div className="col-3"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container px-5">
        {menu === "feed" ? <Feed></Feed>: <></>}
        {menu === "communities" ? <CommList></CommList>: <></>}
        {menu === "friends" ? <FriendList></FriendList>: <></>}
        {menu === "media" ? <Media></Media>: <></>}
        {menu === "events" ? <EventList></EventList>: <></>}
      </div>
    </>
  );
};

export default Profile;
