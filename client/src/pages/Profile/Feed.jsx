import React from "react";

import "./css/feed.css"

import FeedCard from "../../components/Core/Feed/FeedCard";
import NewFeed from "../../components/Core/NewFeed/NewFeed";
import UserDetailCard from "../../components/User/UserDetailCard";

const Feed = () => {
  return (
    <>
      <div className="container px-5">
        <div className="row mt-4">
          <div className="col-lg-4">
            <UserDetailCard></UserDetailCard>
          </div>
          <div className="col-lg-8 message-wrap-xsml" id="style-3">
            <NewFeed></NewFeed>
            <FeedCard></FeedCard>
            <FeedCard></FeedCard>
            <FeedCard></FeedCard>
            <FeedCard></FeedCard>
          </div>
        </div>
      </div>
    </>
  );
};

export default Feed;
