import React from "react";
import { Outlet } from "react-router-dom";

import "./homeLayout.css";

import HomeNavbar from "../../../components/Navbar/HomeNavbar";
import SideNavButton from "../../../components/Core/SideNavButton/SideNavButton";
import SideShortcutButton from "../../../components/Core/SideShortcutButton/SideShortcutButton";

const HomeLayout = () => {
  return (
    <>
      <HomeNavbar></HomeNavbar>
      <div className="container-fluid">
        <div className="row">
          <div className="col-2">
            <SideNavButton
              icon="fa-solid fa-user"
              name="Kadir Umut Sanel"
              link="/profile"
            />
            <SideNavButton
              icon="fa-solid fa-users"
              name="Topluluklar"
              link="/communities"
            />
            <SideNavButton
              icon="fa-solid fa-users-gear"
              name="Topluluk Yönetimi"
              link="/communities/manage"
            />
            <SideNavButton
              icon="fa-solid fa-calendar-day"
              name="Etkinlikler"
              link="/events"
            />
            <SideNavButton
              icon="fa-solid fa-cloud"
              name="Depolarım"
              link="/cloud/my"
            />
            <SideNavButton
              icon="fa-solid fa-cloud-meatball"
              name="Açık Depolar"
              link="/cloud/open"
            />
            <hr />
            <span className="fs-5 w-100">Kısayollarım</span>{" "}
            <button className="btn btn-outline-purple-100 text-dark border-0">
              <i class="fa-solid fa-pen-to-square"></i>
            </button>
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Yazilim ve Donanim arastirma toplulugu"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Raclab"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Raclab"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Raclab"
              link="/community/raclab"
            />
          </div>
          <div className="col-8">
            <div className="container px-5 message-wrap" id="style-3">
              <Outlet></Outlet>
            </div>
          </div>
          <div className="col-2 mt-4">
            <span className="fs-5 w-100">Kişiler</span>
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Kadir Umut Şanel"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Cemil Elçin"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Furkan Çınar"
              link="/community/raclab"
            />
            <SideShortcutButton
              picture="https://picsum.photos/200"
              name="Emir Alanyalıoğlu"
              link="/community/raclab"
            />
            <hr />
            <span className="fs-5 w-100">Grup Konuşmaları</span>
          </div>
        </div>
      </div>
    </>
  );
};

export default HomeLayout;
