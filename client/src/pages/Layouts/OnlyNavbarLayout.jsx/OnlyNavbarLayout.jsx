import React from "react";
import { Outlet } from "react-router-dom";

import HomeNavbar from "../../../components/Navbar/HomeNavbar";

const OnlyNavbarLayout = () => {
  return (
    <>
      <HomeNavbar></HomeNavbar>
      <Outlet></Outlet>
    </>
  );
};

export default OnlyNavbarLayout;
