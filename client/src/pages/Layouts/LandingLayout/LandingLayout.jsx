import React from "react";
import { Outlet } from "react-router-dom";

import Navbar from "../../../components/Navbar/Navbar";
import Footer from "../../../components/Footer/Footer";

const LandingLayout = () => {
  return (
    <>
      <div className="container-fluid p-0 m-0">
        <Navbar></Navbar>
      </div>
      <div className="container mt-5">
        <Outlet />
      </div>
      <div className="container-fluid p-0 m-0 bg-purple-200">
        <Footer></Footer>
      </div>
    </>
  );
};

export default LandingLayout;
