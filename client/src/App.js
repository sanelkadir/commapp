import { Route, Routes, BrowserRouter } from "react-router-dom";
import Cookies from "js-cookie";

import LandingLayout from "./pages/Layouts/LandingLayout/LandingLayout";
import HomeLayout from "./pages/Layouts/HomeLayout/HomeLayout";
import OnlyNavbarLayout from "./pages/Layouts/OnlyNavbarLayout.jsx/OnlyNavbarLayout";
import AuthLayout from "./pages/Layouts/AuthLayout/AuthLayout";
import NoPage from "./pages/NoPage/NoPage";

import Landing from "./pages/Landing/Landing";
import Login from "./pages/Auth/Login/Login";
import Register from "./pages/Auth/Register/Register";

import Home from "./pages/Home/Home";
import Profile from "./pages/Profile/Profile";

function App() {
  if (!Cookies.get("accessToken")) {
    return (
      <BrowserRouter>
        <Routes>
          <Route path="" element={<LandingLayout />}>
            <Route index element={<Landing />} />
          </Route>
          <Route path="/auth" element={<AuthLayout />}>
            <Route path="login" element={<Login />} />
            <Route path="register" element={<Register />} />
          </Route>
          <Route path="*" element={<NoPage />} />
        </Routes>
      </BrowserRouter>
    );
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="" element={<HomeLayout />}>
          <Route index element={<Home />} />
        </Route>
        <Route path="/profile" element={<OnlyNavbarLayout />}>
          <Route index element={<Profile />} />
          <Route path=":id" element={<Profile />} />
        </Route>
        <Route path="*" element={<NoPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
