import Cookies from "js-cookie";

export default class Auth{
  async logOut(){
    console.log("cikis yapilamiyor")
    Cookies.remove('accessToken', { path: '/' })
    Cookies.remove('_id', { path: '/' })
    Cookies.remove('username', { path: '/' })
    Cookies.remove('email', { path: '/' })
    Cookies.remove('isAdmin', { path: '/' })
    Cookies.remove('createdAt', { path: '/' })
    Cookies.remove('updatedAt', { path: '/' })
    Cookies.remove('__v', { path: '/' })
}
}