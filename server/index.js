// Require the modules needed
const express = require("express");
const { graphqlHTTP } = require("express-graphql");

// dotenv is a module that loads environment variables from a .env file into process.env
require("dotenv").config();
const port = process.env.port || 5000;
const app = express();

// Require the schema
const schema = require("./schema/schema");

// express middleware to handle graphql requests
app.use("/graphql", graphqlHTTP({ schema, graphiql: true }));

// start the server
app.listen(port, () => console.log(`API SERVER listening on port ${port}!`));
