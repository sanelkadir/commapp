// Projects
const communities = [
  {
    id: "1",
    userId: ["1"],
    name: "eCommerce Website",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
    status: "In Progress",
  },
  {
    id: "2",
    userId: ["2"],
    name: "Dating App",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
    status: "In Progress",
  },
  {
    id: "3",
    userId: ["4", "5"],
    name: "SEO Project",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
    status: "In Progress",
  },
  {
    id: "4",
    userId: ["4"],
    name: "Design Prototype",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
    status: "Done",
  },
  {
    id: "5",
    userId: ["5"],
    name: "Auction Website",
    description:
      "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu.",
    status: "In Progress",
  },
];

// Posts
const posts = [
  {
    id: "1",
    content: "demo content 1",
    user: "1",
    community: "1",
  },
  {
    id: "2",
    content: "demo content 2",
    user: "4",
    community: "2",
  },
  {
    id: "3",
    content: "demo content 3",
    user: "2",
    community: "3",
  },
  {
    id: "4",
    content: "demo content 4",
    user: "3",
    community: "4",
  },
  {
    id: "5",
    content: "demo content 5",
    user: "5",
    community: "5",
  },
];

// Clients
const users = [
  {
    id: "1",
    name: "Tony",
    surname: "Stark",
    username: "ironman",
    email: "ironman@gmail.com",
    phone: "343-567-4333",
    likes: ["1", "2", "3"],
  },
  {
    id: "2",
    name: "Natasha ",
    surname: "Romanova",
    username: "blackwidow",
    email: "blackwidow@gmail.com",
    phone: "223-567-3322",
    likes: ["4", "3"],
  },
  {
    id: "3",
    name: "Thor ",
    surname: "Odinson",
    username: "thor",
    email: "thor@gmail.com",
    phone: "324-331-4333",
    likes: ["1", "2"],
  },
  {
    id: "4",
    name: "Steve ",
    surname: "Rogers",
    username: "captain",
    email: "steve@gmail.com",
    phone: "344-562-6787",
    likes: ["1", "2", "3"],
  },
  {
    id: "5",
    name: "Bruce ",
    surname: "Banner",
    username: "hulk",
    email: "hulk@gmail.com",
    phone: "321-468-8887",
    likes: ["1", "2", "3"],
  },
];

module.exports = { communities, users, posts };
