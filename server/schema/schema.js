const { communities, users, posts } = require("./sampleData");
const {
  GraphQLObjectType,
  GraphQLID,
  GraphQLString,
  GraphQLSchema,
  GraphQLList,
} = require("graphql");

// User Type
// TODO: add more fields to the user type
// TODO: add a field to the user type that returns the communities that the user is a member of
// TODO: add a field to the user type that returns the communities that the user is the owner of
const UserType = new GraphQLObjectType({
  name: "Users",
  fields: () => ({
    id: { type: GraphQLID },
    username: { type: GraphQLString },
    name: { type: GraphQLString },
    surname: { type: GraphQLString },
    email: { type: GraphQLString },
    phone: { type: GraphQLString },
  }),
});

// Post Type
// TODO: add more fields to the post type
// TODO: add a field to the post type that returns the user that created the post
// TODO: add a field to the post type that returns the communities that the post is a member of
const PostType = new GraphQLObjectType({
  name: "Posts",
  fields: () => ({
    id: { type: GraphQLID },
    content: { type: GraphQLString },
    user: {
      type: UserType,
      resolve: (post) => users.find((user) => user.id === post.user),
    },
    community: {
      type: CommunityType,
      resolve: (post) =>
        communities.find((community) => community.id === post.community),
    },
  }),
});

// Community Type
const CommunityType = new GraphQLObjectType({
  name: "Community",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    description: { type: GraphQLString },
    users: { type: new GraphQLList(UserType), resolve: (community) => community.users },
  }),
});

// Root Query
const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",
  fields: {
    // Get user by id
    user: {
      type: UserType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return users.find((user) => user.id === args.id);
      },
    },
    // Get all users
    users: {
      type: new GraphQLList(UserType),
      resolve(parent, args) {
        return users;
      },
    },
    // Get community by id
    community: {
      type: CommunityType,
      args: { id: { type: GraphQLID } },
      resolve(parent, args) {
        return communities.find((community) => community.id === args.id);
      },
    },
    // Get all communities
    communities: {
      type: new GraphQLList(CommunityType),
      resolve(parent, args) {
        return communities;
      },
    },
    // Get post by id
    post: {
      type: PostType,
      args: {
        id: { type: GraphQLID },
        user: { type: GraphQLID },
        community: { type: GraphQLID },
      },
      resolve(parent, args) {
        return posts.find(
          (post) =>
            post.id === args.id ||
            post.user === args.user ||
            post.community === args.community
        );
      },
    },
    // Get all posts
    posts: {
      type: new GraphQLList(PostType),
      resolve(parent, args) {
        return posts;
      },
    },
  },
});

// Module.exports
module.exports = new GraphQLSchema({
  query: RootQuery,
});
